#!/usr/bin/env python
import csv
import sys
import time
from collections import Counter
from itertools import groupby
from geopy.geocoders import Nominatim


def get_country(address):
    geolocator = Nominatim()
    location = geolocator.geocode(address, timeout=120, language='en')
    time.sleep(1)
    try:
        return location.address.split(', ')[-1]
    except:
        return 'unknown'

cities = []
with open(sys.argv[1], 'rb') as locations:
    reader = csv.reader(locations)
    for loc in reader:
        cities += loc

cities = Counter(cities).most_common()
countries = map(lambda (city, occurrences): (city, occurrences, get_country(city)), cities)
countries.sort(key=lambda c: c[2])
reduced_countries = [
    (country, reduce(lambda x, y: x + y, [e[1] for e in entries])) \
    for country, entries in groupby(countries, lambda c: c[2])
]

for country, occurrences in reduced_countries:
    print "{0} {1}".format(country, occurrences)
